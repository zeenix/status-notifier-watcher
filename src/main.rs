mod watcher;

use watcher::Watcher;

#[async_std::main]
async fn main() -> zbus::Result<()> {
    let watcher = Watcher::new();
    let _conn = watcher.connect().await?;

    loop {
        std::thread::park();
    }
}
